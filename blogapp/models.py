from django.db import models


class Post(models.Model):
	title=models.CharField(max_length=200)
	content=models.TextField()
	posted_by=models.CharField(max_length=100)
	date=models.DateTimeField(auto_now_add=True)
	image=models.ImageField(upload_to='post',null=True,blank=True)


class Comment(models.Model):
	post=models.ForeignKey(Post,on_delete=models.CASCADE)
	comment=models.TextField()
	commented_by=models.CharField(max_length=100)
	date=models.DateTimeField(auto_now_add=True)