from django.urls import path
from .views import *

urlpatterns = [

    path('', HomeView.as_view()),
    path('contact/',ContactView.as_view()),
    path('about/',AboutView.as_view()),
    path('history/',HistoryView.as_view()),
    path('blog/list/',BlogListView.as_view()),
    path('blog/<int:pk>/detail/', BlogDetailView.as_view()),
    path('blogs/create/',PostCreateView.as_view()),
    path('comment/create/',CommentCreateView.as_view()), 
    path('post/<int:pk>/edit/',PostEditView.as_view()), 
    path('post/<int:pk>/del/',PostDeleteView.as_view()), 

]

