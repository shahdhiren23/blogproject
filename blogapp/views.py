from django.views.generic import *
from django.core.mail import send_mail
from django.conf import settings
from .models import Post
from .forms import *

class HomeView(TemplateView):
	template_name='home.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['data_name'] = "I am Dhiren Shah"
		context['age'] = 22
		context['myposts'] = Post.objects.all()

		return context


class ContactView(TemplateView):
	template_name='contact.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['abcdef'] = Post.objects.all()
		return context

class AboutView(TemplateView):
	template_name='about.html'

class HistoryView(TemplateView):
	template_name='history.html'

class BlogListView(ListView):
	template_name='bloglist.html'
	#model=Post
	queryset = Post.objects.all().order_by("-id")
	context_object_name='postlist'


class BlogDetailView(DetailView):
	template_name='blogdetail.html'
	model=Post
	context_object_name='postdetail'

class PostCreateView(CreateView):
     template_name='postcreate.html'
     form_class=PostForm
     success_url='/'

     def form_valid(self,form):
     	a=form.cleaned_data['title']
     	send_mail(
     		'New Post Uploade',
     		'Title:'+a,
     		settings.EMAIL_HOST_USER,
     		['sangit.niroula@gmail.com','ritaraeebantawa@gmail.com'],
     		fail_silently=False


     		)
     	return super().form_valid(form)

class CommentCreateView(CreateView):
     template_name='commentcreate.html'
     form_class=CommentForm
     success_url='/'

class PostEditView(UpdateView):
      template_name='postcreate.html'
      form_class=PostForm
      model=Post
      success_url='/blog/list/'


class PostDeleteView(DeleteView):
	template_name = "blogdelete.html"
	model = Post
	success_url = "/blog/list/"